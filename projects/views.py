from django.shortcuts import redirect
from projects.models import Project
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.urls import reverse_lazy


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    # context_object_name = project_dv


def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    tasks = []
    if self.request.user.is_authenticated:
        for tasks in self.request.tasks.project.all():
            tasks.append(ProjectDetailView)
            tasks.append(tasks.object.all)
    context["tasks_in_project_detail"] = tasks
    return context


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def success_url(self):
        return reverse_lazy("project_listview", args=[self.object.id])

    def form_valid(self, form):
        item = form.save(commit=False)
        item.members = self.request.user
        item.save()
        return redirect("show_project")
