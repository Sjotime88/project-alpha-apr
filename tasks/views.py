from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from tasks.models import Task


class TaskCreateView(LoginRequiredMixin, CreateView):

    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = reverse_lazy("task_list")

    def form_valid(self, form):

        item = form.save(commit=False)
        item.member = self.request.user
        item.save()
        return redirect("projects/detail.html")


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def form_valid(self, form):
        form.instance.task.all = self.request.user
        return super().form_valid(form)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task

    fields = ["is_comlpeted"]
    success_url = reverse_lazy("show_my_tasks")
