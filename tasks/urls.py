from django.urls import path
from tasks.views import TaskCreateView, TaskUpdateView

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
]
