from django import forms
from django.contrib.auth import get_user_model
from models import Signup

User = get_user_model()


class SignupForm(forms.ModelForm):
    username = forms.CharField
    password1 = forms.CharField
    password2 = forms.CharField

    class Meta:
        model = Signup
        fields = ["username", "password"]
