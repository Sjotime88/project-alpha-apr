from django.db import models
from django.conf import settings


class Signup(models.Model):
    username = models.CharField(max_length=150)
    password = models.CharField

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="signup",
        on_delete=models.CASCADE,
    )
